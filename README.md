# Smartcity Micropython core

## Documentation

[see wiki](https://git.skriware.host/alek.dzierzanowski/smartcity-micropython/wikis/home)

## Setup

Install libs required for development:

```bash
$ pip3 install -r requirements.txt
```

## Build

* clean the MCU's flash

```bash
$ esptool --port /dev/cu.usbserial-DN05TISB erase_flash
```

* upload the appropriate `firmware.bin` using `esptool.py`

```bash
$ esptool.py \
  --chip esp32 \
  --port /dev/cu.usbserial-DN05TISB \
  --baud 921600 \
  --before default_reset \
  --after hard_reset \
  write_flash -z \
  --flash-mode dio \
  --flash-freq 80m \
  --flash-size detect \
  0x1000 firmware.bin
```

* upload certain modules to the board, e.g.:

```bash
$ python3 upload.py --port /dev/cu.usbserial-DN05TISB --mode master
```

## Testing

To test the system, upload using `--tests` switch:

```bash
$ ./upload.py --port /dev/cu.usbserial-DN05TISB --mode master --tests
```

Then, connect to the board and type:

```python
>>> import tests
>>> tests.run_all()
```
