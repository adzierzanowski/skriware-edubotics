from http.http_server import HTTPServer

import network

import _thread
from device.dht_node import DHTNode
from device.node import Node
from machine import reset
from utils import config, log

iface = network.WLAN(network.STA_IF)
iface.active(True)

node = DHTNode(iface, network_color=config.default_network_color)
http_server = HTTPServer(iface, mode='node', device=node)

_thread.start_new_thread(http_server.listen, ())
log.debug('HTTP server started in a separate thread')
