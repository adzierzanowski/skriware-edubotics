from http.http_server import HTTPServer

from network import STA_IF, WLAN

import _thread
from device.master import Master
from machine import reset
from utils import config, log
from utils.ping import ping
from module.types import *

try:
  from device import credentials
except ImportError:
  credentials = None

config.queue_logs = True

master = None

if __name__ == '__main__':
  master = Master(network_color=config.default_network_color)
  config.master = master
  ap = master.create_ap()
  http_server = HTTPServer(ap, mode='master', device=master)

  _thread.start_new_thread(http_server.listen, ())
  log.debug('HTTP server started in a separate thread')

  if credentials and False:
    sta = WLAN(STA_IF)
    sta.active(True)
    sta.connect(credentials.essid, credentials.password)
