from http.http_server import HTTPServer

import network

import _thread
from device.pressure_sensor_node import PressureSensorNode
from device.node import Node
from machine import reset
from utils import config, log

iface = network.WLAN(network.STA_IF)
iface.active(True)

node = PressureSensorNode(iface, network_color=config.default_network_color)
http_server = HTTPServer(iface, mode='node', device=node)

_thread.start_new_thread(http_server.listen, ())
log.debug('HTTP server started in a separate thread')
