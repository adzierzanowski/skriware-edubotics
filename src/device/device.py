import gc
import os
from binascii import hexlify

from machine import unique_id
from utils import config, log


class Device:
  def __init__(self, network_color=config.default_network_color):
    self.network_color = network_color
    self.getters = ['uid', 'free_fs', 'free_ram', '_debug_n_increment']
    self.debug_n = 0

  def get(self, name):
    if name in self.getters:
      return getattr(self, name)()
    return None

  def _register_getter(self, name):
    if name not in self.getters:
      self.getters.append(name)
      log.debug('Registered getter: {}'.format(name))

  def uid(self):
    return hexlify(unique_id()).decode('ascii')

  def free_fs(self):
    stat = os.statvfs('/')
    total = stat[0] * stat[2]
    free = stat[1] * stat[3]

    return '{:0.0f} kB free of {:0.0f} kB total ({} %)'.format(
      free / 1024,
      total / 1024,
      int(free / total * 100)
    )

  def free_ram(self):
    return '{:0.2f} kB free'.format(gc.mem_free() / 1024)

  def _debug_n_increment(self):
    self.debug_n += 1
    return 'n={}'.format(self.debug_n)
