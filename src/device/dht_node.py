import time

import _thread
from device.node import Node
from dht import DHT11
from machine import Pin
from utils import log


class DHTNode(Node):
  def __init__(self, *args, **kwargs):
    super().__init__(*args, type_='DHT', **kwargs)
    time.sleep(3.0)
    self.dht = DHT11(Pin(2))
    time.sleep(3.0)
    self._register_getter('temperature')
    self._register_getter('humidity')
    _thread.start_new_thread(self._measure, ())

  def _measure(self):
    while True:
      try:
        self.dht.measure()
      except OSError as e:
        log.log_exception(e, __file__)
        log.error('Check if DHT sensor is connected.')
      time.sleep(1.0)

  def temperature(self):
    return '{:0.2f} deg. C'.format(self.dht.temperature())

  def humidity(self):
    return '{:0.0f} %'.format(self.dht.humidity())
