import time

import esp32
import _thread
from device.node import Node
from machine import ADC, Pin
from utils import log


class HallSensorNode(Node):
  def __init__(self, *args, **kwargs):
    super().__init__(*args, type_='Hall Sensor', **kwargs)
    self._register_getter('read')
    self.sensor_value = 0
    _thread.start_new_thread(self._measure, ())

  def _measure(self):
    while True:
      self.sensor_value = esp32.hall_sensor()
      time.sleep(1.0)

  def read(self):
    return 'Magnetic field: {}'.format(self.sensor_value)
