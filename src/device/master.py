import gc
import time

import network

import _thread
from device.device import Device
from device.vnode import VNode
from machine import Pin, unique_id
from neopixel import NeoPixel
from utils import config, log
from utils.ping import ping
from utils.status import status


class Master(Device):
  def __init__(self, network_color=config.default_network_color):
    super().__init__(network_color=network_color)
    self.nodes = {}
    self._nodes_lock = _thread.allocate_lock()
    self._modules = []
    self._modules_lock = _thread.allocate_lock()

    self._iface = None

    if config.queue_logs:
      self._register_getter('logs')

    status.connected(self.network_color)
    status.master()

    _thread.start_new_thread(self._connection_doctor, ())

  def get_nodes(self):
    with self._nodes_lock:
      return self.nodes

  def scan(self):
    '''Prints and returns a list of VNode objects.'''

    VNode._vnodes = []
    self.get_nodes()
    with self._nodes_lock:
      for ip, node_data in self.nodes.items():
        vnode = VNode(node_data, master=self)
        setattr(self, vnode._varname, vnode)
        print(vnode)

    self.scan_modules()

  def scan_modules(self):
    with self._modules_lock:
      self._modules = []

    for vnode in VNode._vnodes:
      with self._modules_lock:
        self._modules += vnode.modules()

  def modules(self, requested_module=None):
    with self._modules_lock:
      if requested_module is None:
        return self._modules
      else:
        return [mod for mod in self._modules if requested_module.TYPE == mod.TYPE]

  def validate(self, module_list):
    '''Returns True if modules from `module_list` are available on the device.'''
    available_modules = [mod.TYPE for mod in self.modules()]
    requested_modules = [mod.TYPE for mod in module_list]
    for mod in requested_modules:
      if available_modules.count(mod) < requested_modules.count(mod):
        return False
    return True

  def _register_node(self, name, ip, data) -> bool:
    '''
    Returns True  if registered succesfully,
            False if node is already registered.
    '''

    with self._nodes_lock:
      if self.nodes.get(ip):
        return False

      for ip_, existing_node in self.nodes.items():
        if ip_ == ip:
          return False

        if existing_node['name'] == name:
          del self.nodes[ip_]
          break

      self.nodes[ip] = data

      return True

  def _connection_doctor(self):
    while True:
      gc.collect()

      with self._nodes_lock:
        for ip, node_data in self.nodes.items():
          if not ping(ip, iface=self._iface):
            log.debug('Connection lost: {} ({})'.format(ip, node_data['name']))
            del self.nodes[ip]

      time.sleep(config.doctor_interval)

  def create_ap(self):
    sta = network.WLAN(network.STA_IF)
    sta.active(False)

    ap = network.WLAN(network.AP_IF)
    ap.active(False)

    ap.config(essid='{}-{}'.format(config.master_prefix, self.uid()))
    ap.config(max_clients=10)
    ap.active(True)

    self._iface = ap
    return ap

  def logs(self):
    loglines = []
    while not log.log_q.empty():
      loglines.append(log.log_q.get())
    return '\n'.join(loglines)
