from device.node import Node
from module.module import ModuleMode
from module.ir import IRModule


class MockIRNode(Node):
  def __init__(self, *args, **kwargs):
    super().__init__(*args, type_='Mock IR', **kwargs)

  def scan_modules(self):
    self._modules = [IRModule(16, mode=ModuleMode.MOCK)]
    return self._modules
