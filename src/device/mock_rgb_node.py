from device.node import Node
from module.module import ModuleMode
from module.rgb import RGBModule


class MockRgbNode(Node):
  def __init__(self, *args, **kwargs):
    super().__init__(*args, type_='Mock RGB', **kwargs)

  def scan_modules(self):
    self._modules = [RGBModule(16, mode=ModuleMode.MOCK)]
    return self._modules
