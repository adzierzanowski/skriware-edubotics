import time

import _thread
from device.node import Node
from machine import ADC, Pin
from utils import log


class MQ7Node(Node):
  def __init__(self, *args, **kwargs):
    super().__init__(*args, type_='MQ-7', **kwargs)
    self._register_getter('analog_read')
    self._register_getter('digital_read')
    self.adc = ADC(Pin(32))
    self.digital_pin = Pin(15)
    self.analog_value = 0
    self.digital_value = 0
    _thread.start_new_thread(self._measure, ())

  def _measure(self):
    while True:
      self.analog_value = self.adc.read()
      self.digital_value = self.digital_pin.value()
      time.sleep(1.0)

  def analog_read(self):
    return 'Analog: {}'.format(self.analog_value)

  def digital_read(self):
    return 'Digital: {}'.format(self.digital_value)
