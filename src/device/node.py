import json
import time
from http.http import MIME, HTTPRequest, request

import _thread
from device.device import Device
from utils import config, edunet, log
from utils.ping import ping
from utils.status import status
from module.communication import scan as modscan
from module.types import MODULE_TYPES


class Node(Device):
  def __init__(self, iface, network_color=config.default_network_color, type_=None):
    super().__init__(network_color=network_color)
    self.iface = iface
    self.connected = False
    self.type = '(none)' if type_ is None else type_
    self.subroutine = None
    self.subroutines = []
    self.sh_lock = _thread.allocate_lock()
    self._modules = []

    status.disconnected(self.network_color)
    status.node()

    _thread.start_new_thread(self._connection_doctor, ())
    _thread.start_new_thread(self._subroutine_handler, ())
    _thread.start_new_thread(self._module_scanner, ())

  def set_subroutine(self, subname):
    with self.sh_lock:
      if subname == 'NONE':
        self.subroutine = None
        return True
      elif subname in self.subroutines:
        self.subroutine = getattr(self, subname)
        return True
      return False

  def register_subroutine(self, name):
    if name not in self.subroutines:
      self.subroutines.append(name)
      log.debug('Registered subroutine {}'.format(name))

  def _subroutine_handler(self):
    while True:
      if self.subroutine is None:
        time.sleep(0.1)
      else:
        self.subroutine()

  def _module_scanner(self):
    while True:
      self.scan_modules()
      time.sleep(5)

  def set_network_color(self, color):
    current = self.network_color
    self.network_color = edunet.colors.get(color, current)

    if self.network_color == current:
      return
    self._find_master()

  def _connection_doctor(self):
    conncnt = 0
    while True:
      conncnt += 1
      try:
        if self.connected:
          if not ping(config.master_ip, iface=self.iface):
            log.debug('Lost connection with the master')
            status.disconnected(self.network_color)
            self.connected = False
            self._find_master()
          elif conncnt % 10 == 0:
            self.register()
        else:
          self._find_master()
      except Exception as e:
        log.log_exception(e, __file__)
        pass
      time.sleep(config.doctor_interval)

  def _find_master(self):
    status.disconnected(self.network_color)
    networks = [
      net[0] for net in self.iface.scan() if net[0].startswith(config.master_prefix)
    ]

    if not networks:
      log.debug('No master networks found')
      self.connected = False
      return

    for net in networks:
      self.iface.disconnect()
      while self.iface.isconnected():
        pass
      self.iface.connect(net)
      while not self.iface.isconnected():
        pass

      res = request(
        config.master_ip,
        self.iface,
        HTTPRequest(config.master_ip, route='/network-color')
      )
      network_color = res.body().decode('ascii')

      if network_color == self.network_color:
        self.connected = True
        status.connected(self.network_color)
        self.register()
        return

      self.iface.disconnect()
      self.connected = False

  def register(self):
    body = {
      'name': self.uid(),
      'ip': self.iface.ifconfig()[0],
      'type': self.type
    }

    req = HTTPRequest(
      host=config.master_ip,
      method='POST',
      route='/register',
      mime_type=MIME['json'],
      body=json.dumps(body)
    )

    try:
      res = request(config.master_ip, self.iface, req)
    except OSError:
      pass

  def scan_modules(self):
    self._modules = [
      MODULE_TYPES[mod.get('type', 0xff)](mod.get('pin')) for mod in modscan()]
    return self._modules
