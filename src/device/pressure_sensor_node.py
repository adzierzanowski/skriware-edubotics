import time

import _thread
from device.node import Node
from machine import ADC, Pin
from utils import log


class PressureSensorNode(Node):
  def __init__(self, *args, **kwargs):
    super().__init__(*args, type_='Pressure Sensor', **kwargs)
    self._register_getter('analog_read')
    self._register_getter('percentage_read')
    self.adc = ADC(Pin(36))
    self.analog_value = 0
    self.digital_value = 0
    _thread.start_new_thread(self._measure, ())

  def _measure(self):
    while True:
      self.analog_value = self.adc.read()
      time.sleep(1.0)

  def analog_read(self):
    return 'Analog: {}'.format(self.analog_value)

  def percentage_read(self):
    return 'Percentage: {}%'.format(int(self.analog_value / 4096.0 * 100))
