import json
from http.http import request, HTTPRequest, HTTPResponse
from module.module import ModuleMode
from module.types import MODULE_TYPES


class VNode:
  '''An abstraction over a connected node meant to be a user interface for
  performing actions via CLI.'''

  _vnodes = []

  def __init__(self, node_data, master=None):
    self.ip = node_data.get('ip')
    self.uid = node_data.get('name')
    self.type = node_data.get('type')
    self._master = master
    self._methods = []
    self._modules = []

    self._vnodes.append(self)

    self._raw_varname = self.type.replace(' ', '_').lower()
    raw_varnames = [vnode._raw_varname for vnode in VNode._vnodes]
    self._varname = '{}{}'.format(
      self._raw_varname, raw_varnames.count(self._raw_varname))

    self._available()
    self._get_modules()

  def __repr__(self):
    return 'Node: {} = {} ({})'.format(self._varname, self.type, self.ip)

  def _request(self, route, body=None):
    try:
      req = HTTPRequest(host=self.ip, route=route, body=body)
      res = request(self.ip, self._master._iface, req)
      return res
    except OSError:
      body = '{} is unreachable'.format(self._varname)
      return HTTPResponse(status=408, body=body)

  def _available(self):
    res = self._request('/available')

    available_methods = json.loads(res.body()).get('getters', [])
    for meth in available_methods:
      # It's not very pretty, it assigns an endpoint to a method of the VNode
      # object.
      if not getattr(self, meth, None):
        self._methods.append(meth)
        setattr(self, meth,
          lambda meth=meth: self._request('/get/{}'.format(meth)).body().decode('utf-8'))

  def _get_modules(self):
    # TODO: request for modules, assign modules to the vnode
    res = self._request('/modules')
    modules = json.loads(res.body())

    self._modules = []
    for mod in modules:
      type_ = mod.get('type', 0xff)
      pin = mod.get('pin', 0)
      ModClass = MODULE_TYPES[type_]
      instance = ModClass(pin, mode=ModuleMode.REMOTE, vnode=self)
      self._modules.append(instance)

  def available(self):
    for meth in self._methods:
      print(meth)

  def modules(self):
    self._get_modules()
    for i, mod in enumerate(self._modules):
      print('Module: {} ({})'.format(mod.NAME, mod.vnode._varname))
    return self._modules

  def ping(self):
    res = self._request('/ping')
    return res.body() == b'pong'
