import time

from utils.config import master
from module.types import *


master.scan()

if master.validate([IR_MODULE, IR_MODULE, IR_MODULE, LED_MODULE, RGB_MODULE]):
    irs = master.modules(IR_MODULE)
    leds = master.modules(LED_MODULE)
    rgbs = master.modules(RGB_MODULE)

    pedestrian_entry_ir = irs[0]
    pedestrian_pass_ir = irs[1]
    vehicle_ir = irs[2]
    traffic_lights = rgbs[0]
    pedestrian_led = leds[0]

    pedestrian_entry_ir.on()
    pedestrian_pass_ir.on()
    vehicle_ir.on()

    traffic_lights.set_pixel(0, 0xff, 0, 0)
    traffic_lights.update()
    vehicle_green_light = False
    change_vehicle_light = False
    change_vehicle_light_time = time.time()
    change_vehicle_light_timeout = 2

    while True:
        if not pedestrian_entry_ir.read():
            pedestrian_led.on(1)

        if not pedestrian_pass_ir.read():
            pedestrian_led.off(1)

        if not vehicle_ir.read():
            change_vehicle_light = True
            change_vehicle_light_time = time.time()
            change_vehicle_light_timeout = 2

        if change_vehicle_light:
            if time.time() - change_vehicle_light_time > change_vehicle_light_timeout:
                if vehicle_green_light:
                    traffic_lights.clear()
                    traffic_lights.set_pixel(0, 0xff, 0, 0)
                    traffic_lights.update()
                    vehicle_green_light = False
                    change_vehicle_light = False
                else:
                    traffic_lights.clear()
                    traffic_lights.set_pixel(2, 0, 0xff, 0)
                    traffic_lights.update()
                    vehicle_green_light = True
                    change_vehicle_light_timeout = 5
                    change_vehicle_light_time = time.time()
