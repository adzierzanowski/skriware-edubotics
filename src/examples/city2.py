import time

from utils.config import master
from module.types import *


master.scan()

if master.validate([LED_MODULE, RGB_MODULE]):
    led = master.modules(LED_MODULE)[0]
    rgb = master.modules(RGB_MODULE)[0]

    rgb.set_pixel(0, 0xff, 0, 0)
    rgb.update()

    # pieszy zbliża się do przejścia
    # (od razu przy starcie programu sygnalizacja przejścia pieszego się włącza)
    led.on(1)
    # pieszy przechodzi
    time.sleep(2)
    led.off(1)

    # pojazd podjeżdża do świateł
    time.sleep(10)

    # zmiana świateł na zielone
    rgb.set_pixel(1, 0xff, 0xff, 0)
    rgb.update()
    time.sleep(1)

    rgb.clear()
    rgb.set_pixel(2, 0, 0xff, 0)
    rgb.update()

    # pojazd jedzie dalej
    time.sleep(5)

    # zmiana świateł na czerwone
    rgb.clear()
    rgb.set_pixel(1, 0xff, 0xff, 0)
    rgb.update()
    time.sleep(1)

    rgb.clear()
    rgb.set_pixel(0, 0xff, 0, 0)
    rgb.update()
