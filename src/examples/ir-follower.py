'''
This script fetches the IR signal. If such is present, it turns the LEDs on,
otherwise - it turns them back off.
'''

from utils.config import master
from module.types import *

master.scan()

if master.validate([IR_MODULE, LED_MODULE]):
    # Two lists of connected IR and LED modules
    irs = master.module(IR_MODULE)
    leds = master.module(LED_MODULE)

    # Get the first module from each of the lists
    ir = irs[0]
    led = leds[0]

    # powyższe można zapisać po prostu
    #    ir = master.module(IR_MODULE)[0]
    #    led = master.module(LED_MODULE)[0]
    # ale tak jest chyba czytelniej

    while True:
        if ir.read():
            led.on(1) # Turn LED at port #1 on
        else:
            led.off(1) # Turn LED at port #1 off

else:
    print('Some of the modules are missing.')
