import time

from utils.config import master
from module.types import *


master.scan()

if master.validate([RGB_MODULE]):
    rgbs = master.modules()
    rgb = rgbs[0]

    while True:
        # Ustawiamy światła na czerwone:
        # LED 0: czerwony
        # LED 1: brak
        # LED 2: brak
        rgb.reset()
        rgb.set_pixel(0, 0xff, 0, 0)
        rgb.update()

        # Czekamy 10 sekund
        time.sleep(10)

        # Dodajemy żółte światło
        # 0 czerwony
        # 1 żółty
        # 2 brak
        rgb.set_pixel(1, 0xff, 0xff, 0)
        rgb.update()

        # Czekamy 2 sekundy
        time.sleep(2)

        # Ustawiamy zielone światło
        # 0 brak
        # 1 brak
        # 2 zielony
        rgb.clear()
        rgb.set_pixel(2, 0, 0xff, 0)
        rgb.update()

        # Czekamy 10 sekund
        time.sleep(10)

        # Ustawiamy żółte światło
        # 0 brak
        # 1 żółty
        # 2 brak
        rgb.clear()
        rgb.set_pixel(1, 0xff, 0xff, 0)
        rgb.update()

        # Czekamy 2 sekundy i zaczynamy od nowa
        time.sleep(2)

else:
    print('LED module is missing.')
