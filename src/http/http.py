import socket

from utils import ansi, config

STATUS = {
  200: '200 OK',
  301: '301 Moved Permanently',
  400: '400 Bad Request',
  401: '401 Unauthorized',
  403: '403 Forbidden',
  404: '404 Not Found',
  408: '408 Request Timeout',
  418: '418 I\'m a teapot',
  500: '500 Internal Server Error'
}

MIME = {
  'plain': 'text/plain',
  'html': 'text/html',
  'css': 'text/css',
  'js': 'text/javascript',
  'json': 'application/json',
  'png': 'image/png',
  'bytes': 'application/octet-stream'
}


class HTTPFrame:
  def __init__(self, headers=None, body=None, mime_type=MIME['plain']):
    self._headers = headers
    self._body = body
    self._mime_type = mime_type

  def __str__(self):
    return self.raw().decode('ascii')

  def header_list(self):
    hdrs = {
      'Content-Type': self._mime_type,
      'Content-Length': len(self.body())
    }

    if self._headers is not None:
      for k, v in self._headers.items():
        hdrs[k] = v

    if isinstance(self, HTTPRequest):
      if 'Host' not in hdrs:
        hdrs['Host'] = self._host

    out = []
    for k, v in hdrs.items():
      out.append('{}: {}'.format(k, v))

    return out

  def body(self):
    if self._body is None:
      return b''

    if type(self._body) == bytes:
      return self._body

    return self._body.encode('ascii')

  @staticmethod
  def parse(raw):
    data = raw.decode('ascii')
    header = data.split('\r\n\r\n')[0]
    body = '\r\n'.join(data.split('\r\n\r\n')[1:])

    hsplit = header.split('\r\n')
    status_line = hsplit[0].split(' ')
    headers = dict([h.split(': ') for h in hsplit[1:]])

    if status_line[0].startswith('HTTP'):
      frame = HTTPResponse()
      frame._status = int(status_line[1])

    else:
      frame = HTTPRequest()
      method = status_line[0]
      route = status_line[1]

      frame._host = headers['Host']
      frame._method = method
      frame._route = route

    frame._headers = headers
    frame._body = body
    if 'Content-Type' in headers:
      frame._mime_type = headers['Content-Type']

    return frame

  def raw(self):
    return '\r\n'.join(
      [self.status_line()] + self.header_list() + ['', '']
    ).encode('ascii') + self.body()

  def status_line(self):
    raise NotImplementedError()

class HTTPRequest(HTTPFrame):
  def __init__(self, host='127.0.0.1', method='GET', route='/', **kwargs):
    super().__init__(**kwargs)
    self._host = host
    self._method = method
    self._route = route

  def __repr__(self):
    return '{} {}'.format(
      ansi.fg(11, '{:5}'.format(self._method)),
      ansi.fg(10, self._route)
    )

  @property
  def route(self):
    return self._route

  @property
  def method(self):
    return self._method

  def status_line(self):
    return '{} {} HTTP/1.1'.format(self._method, self._route)

  def route_params(self):
    params = {}

    split_route = self.route.split('?')
    if len(split_route) > 1:
      split_params = ('?'.join(split_route[1:])).split('&')
      for kv in split_params:
        k, v = kv.split('=')
        params[k] = v

    return params

  def route_without_params(self):
    return self.route.split('?')[0]

class HTTPResponse(HTTPFrame):
  def __init__(self, status=200, **kwargs):
    super().__init__(**kwargs)
    self._status = status

  def __repr__(self):
    return '{} {}'.format(
      ansi.fg(11, '{:5}'.format(self._status)),
      ''
    )

  def status_line(self):
    return 'HTTP/1.1 {}'.format(self._status)

  @staticmethod
  def status_response(status, msg=None):
    body = '<h1>{}</h1>'.format(STATUS[status])
    if msg:
      body += '<p>{}</p>'.format(msg)

    return HTTPResponse(status=status, body=msg, mime_type=MIME['html'])

  @staticmethod
  def redirect(dst):
    return HTTPResponse(status=301, headers={'Location': dst})

def request(host, iface, req: HTTPRequest, timeout=2.0) -> HTTPResponse:
  '''Send a request using a socket'''

  s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
  s.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
  s.settimeout(timeout)
  s.connect((host, 80))
  s.send(req.raw())
  res = s.recv(config.data_limit)
  s.close()
  return HTTPResponse.parse(res)
