import gc
import json
import os
import socket
from http import http

import network

import _thread
from machine import Pin, reset
from utils import ansi, config, log
from utils.server import Server

class HTTPServer(Server):
  def __init__(self, iface, mode='node', device=None):
    super().__init__(iface, client_handler=self.handle_request_wrapper, port=80)
    self.mode = mode
    self.device = device

  def redirect_request(self, conn, addr, req, dst_addr):
    try:
      s = socket.socket(socket.AF_INET, socket.SOCK_STREAM, 0)
      s.connect((dst_addr, 80))
      s.send(req)
      response = s.recv(config.data_limit)
      conn.send(response)
      conn.close()
    except OSError as e:
      log.log_exception(e, __file__)
      self.errcnt += 1
    else:
      self.errcnt = 0
    finally:
      conn.close()

    return None

  def handle_request_wrapper(self, conn, addr):
    gc.collect()

    response = None
    raw_request = conn.recv(config.data_limit)

    try:
      response = self.handle_request(conn, addr, raw_request)
      if response is not None:
        conn.send(response.raw())
    except MemoryError as e:
      gc.collect()
      log.log_exception(e, __file__)
      response = http.HTTPResponse.status_response(500)
      conn.send(response.raw())
      self.errcnt += 1
    else:
      self.errcnt = 0
    finally:
      conn.close()

  def handle_request(self, conn, addr, raw_request):
    try:
      req = http.HTTPRequest.parse(raw_request)

    except Exception as e:
      log.exception(e, __file__)
      log.error('req: {}'.format(raw_request))
      self.errcnt += 1
      return http.HTTPResponse.status_response(500)
    else:
      self.errcnt = 0

    host, port = addr
    log.debug('{}req {}:{} {}'.format(ansi.end, host, port, repr(req)))

    if req.route == '/':
      res = http.HTTPResponse.redirect('/index.html')
      log.log_http_response(res, host, port)
      return res

    params = req.route_params()
    self_ip = self.iface.ifconfig()[0]
    if params.get('redirect', self_ip) != self_ip:
      return self.redirect_request(conn, addr, req.raw(), params['redirect'])

    # Ping
    if req.route.startswith('/ping'):
      return self.handle_ping(req.route_without_params, conn, addr)

    # Set subroutine to be executed in the worker thread
    if req.route.startswith('/set/') and req.method == 'POST':
      return self.handle_set(req.route_without_params(), conn, addr)

    # Get subroutine's returned data
    if req.route.startswith('/get/'):
      return self.handle_get(req.route_without_params(), conn, addr)

    if req.route.startswith('/available'):
      return self.handle_available(req.route_without_params(), conn, addr)

    # Get registered nodes
    if req.route.startswith('/nodes'):
      return self.handle_nodes(req.route, conn, addr)

    # Get connected modules
    if req.route.startswith('/modules'):
      return self.handle_modules(req.route, conn, addr)

    # Send frame to a selected module
    if req.route.startswith('/send-frame'):
      return self.handle_send_frame(
        req.route_without_params(), conn, addr, req.body())

    # 418 drink it
    if req.route.startswith('/teapot'):
      res = http.HTTPResponse.status_response(418)
      log.log_http_response(res, host, port)
      return res

    if req.route.startswith('/register') and req.method == 'POST':
      return self.handle_register(req.route, conn, addr, req.body())

    if req.route.startswith('/network-color'):
      return self.handle_network_color(req.route, conn, addr)

    # If every other route match fails,
    # try to find a matching file on the VFS
    return self.handle_resource(req.route, conn, addr)

  def handle_network_color(self, route, conn, addr):
    res = http.HTTPResponse(
      body=self.device.network_color
    )
    log.log_http_response(res, *addr)
    return res

  def handle_ping(self, route, conn, addr):
    res = http.HTTPResponse(body=b'pong')
    log.log_http_response(res, *addr)
    return res

  def handle_nodes(self, route, conn, addr):
    if self.mode == 'master':
      body = json.dumps(self.device.get_nodes())
      log.debug('self.device.get_nodes() = {}'.format(self.device.get_nodes()))
      log.debug('nodes {}'.format(body))
      res = http.HTTPResponse(body=body, mime_type=http.MIME['json'])

    else:
      res = http.HTTPResponse(status=400, body='This is not a master device.')

    log.log_http_response(res, *addr)
    return res

  def handle_modules(self, route, conn, addr):
    if self.mode == 'node':
      body = json.dumps([m._serialize() for m in self.device._modules])
      res = http.HTTPResponse(body=body, mime_type=http.MIME['json'])

    else:
      res = http.HTTPResponse(status=400, body='This is a master device.')

    log.log_http_response(res, *addr)
    return res

  def handle_send_frame(self, route, conn, addr, body):
    if self.mode == 'node':
      data = json.loads(body)

      cs = int(data.get('pin', '-1'))
      frame = data.get('frame')

      try:
        mod = [m for m in self.device._modules if m.cs_num == cs][0]
        modres = mod.send_frame(*frame)
        res = http.HTTPResponse(status=200, body=json.dumps(modres))

      except IndexError:
        res = http.HTTPResponse(
          status=400, body='No module at pin {}.'.format(cs))

      if any([x is None for x in (cs, )]):
        res = http.HTTPResponse(status=400, body='Bad params for a frame.')

    else:
      res = http.HTTPResponse(status=400, body='This is a master device.')

    log.log_http_response(res, *addr)
    return res

  def handle_register(self, route, conn, addr, body):
    if self.mode == 'master':
      try:
        data = json.loads(body)
        ip, name = data.get('ip', None), data.get('name', None)

        if not all((name, ip)):
          return http.HTTPResponse.status_response(
            400, 'Body does not contain required fields'
          )

        # get rid of bytes literal artifacts (b'')
        name = name[1:].replace("'", '')

        if self.device._register_node(name, ip, data):
          log.debug('Registered node with IP: {}'.format(ip))
          res = http.HTTPResponse(body='Node registered successfully')
        else:
          res = http.HTTPResponse(body='Node already registered')

      except Exception as e:
        log.log_exception(e, __file__)
        res = http.HTTPResponse.status_response(500)
        self.errcnt += 1

      else:
        self.errcnt = 0

    else:
      res = http.HTTPResponse.status_response(
        400, 'This is not a master device.'
      )

    log.log_http_response(res, *addr)
    return res

  def handle_set(self, route, conn, addr):
    sub = route.split('/')[2]
    body = 'subroutine {}'.format(sub)
    status = 200

    if self.mode == 'master':
      res = http.HTTPResponse.status_response(400, 'The device should be node.')
    else:
      if self.device.set_subroutine(sub):
        body += ' set successfully'
      else:
        body = 'could not set ' + body
        status = 404

      res = http.HTTPResponse(status=status, body=body)

    log.log_http_response(res, *addr)
    return res

  def handle_get(self, route, conn, addr):
    get_param = route.split('/')[2]
    body = get_param
    status = 200

    if self.device:
      retval = self.device.get(get_param)
      if retval:
        body = retval
      else:
        body += '{} not found'.format(get_param)
        status = 404
      res = http.HTTPResponse(status=status, body=body)
    else:
      res = http.HTTPResponse.status_response(500, 'Bad device')

    log.log_http_response(res, *addr)
    return res

  def handle_available(self, route, conn, addr):
    available = {}
    if hasattr(self.device, 'subroutines'):
      available['subroutines'] = self.device.subroutines
    available['getters'] = self.device.getters

    body = json.dumps(available)
    res = http.HTTPResponse(status=200, body=body, mime_type=http.MIME['json'])

    log.log_http_response(res, *addr)
    return res

  def handle_resource(self, route, conn, addr):
    status = None
    body = b''
    mime_type = http.MIME['plain']

    try:
      os.stat(config.public_dir + route)

      ext = route.split('.')[-1]
      mime_type = http.MIME.get(ext, http.MIME['plain'])

      status = http.STATUS[200]
      with open(config.public_dir + route, 'rb') as f:
        body = f.read()

    except OSError as e:
      status = http.STATUS[404]
      body = '<h1>{}</h1>'.format(status)
      body += '<p>The requested route '
      body += '(<span style="font-family: monospace;">{}</span>) '.format(route)
      body += 'was not found.</p>'
      mime_type = http.MIME['html']
      log.log_exception(e, __file__)
      errcnt += 1

    else:
      self.errcnt = 0

    finally:
      res = http.HTTPResponse(status=status, body=body, mime_type=mime_type)
      log.log_http_response(res, *addr)
      return res
