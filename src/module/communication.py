'''This module handles communication between the node and its modules
In other words: STM32 <-> ESP32 communication'''

from machine import Pin, I2C, SPI


scl, sda = Pin(21), Pin(19)
sck, mosi, miso = Pin(13), Pin(15), Pin(14)

css_raw = (16,17,18)
css = [Pin(x, Pin.OUT) for x in css_raw]

i2c = I2C(scl=scl, sda=sda)
spi = SPI(sck=sck, mosi=mosi, miso=miso)

# NOTE: this should probably be removed
DEVICE_TYPES = {
  0x10: 'WS2812B (Small head)',
  0x11: 'QTR (Reflectance sensor)',
  0x12: 'HC-SR04 (Ultrasonic sensor)',
  0x13: 'Gripper module',
  0x14: 'DC Motor',
  0x15: 'BH1750 (Light sensor)',
  0x16: 'Buzzer',
  0x17: 'LED Matrix',
  0x18: 'Button',
  0x19: 'Infrared',
  0x1a: 'Color sensor',
  0x1b: 'Flow sensor',
  0x1c: 'MQ-type sensor',
  0x1d: 'OLED display',
  0x1e: 'Switch',
  0x1f: 'LED',
  0xff: 'STM32F030',
}


class ChecksumError(Exception):
  def __init__(self, msg):
    super().__init__(msg)

def send_frame(iface, addr, flen, cmd, *args):
  #print('iface:',iface,'addr:',addr,'flen:',flen,'cmd:',cmd,'args:',args) # NOTE: delete
  rxlen, txlen = flen >> 4, flen & 0x0f

  checksum = flen ^ cmd
  for arg in args:
    checksum ^= arg
  checksum += 4

  '''
  print('addr={} rxlen={} txlen={} args={} check={}'.format(
    addr, rxlen, txlen, args, checksum))
  '''

  outbuf = [flen, cmd] + list(args) + [checksum]

  if isinstance(iface, I2C):
    for byte in outbuf:
      iface.writeto(addr, bytes([byte]))
    res = list(iface.readfrom(addr, rxlen+1))

  elif isinstance(iface, SPI):
    # In this case `addr` should be a Pin instance representing CS pin of SPI
    addr.off()
    for byte in outbuf:
      #print('Sending {}'.format(byte))
      iface.write(bytes([byte]))
    res = list(iface.read(rxlen+1))
    #print('Received {}'.format(res))
    addr.on()

  rx_checksum = res[-1] # received checksum
  cs = 0 # calculated checksum
  for byte in res[:-1]:
    cs ^= byte
  cs += 4

  if cs != rx_checksum:
    raise ChecksumError(
      'Bad checksum received: {}, should be {}'.format(rx_checksum, cs))
  return res[:-1]

def get_type(iface, addr):
  type_ = send_frame(iface, addr, 0x10, 0x01)[0]
  try:
    return DEVICE_TYPES[type_]
  except KeyError:
    return 'unknown'

def scan():
  mods = []
  for cs in css_raw:
    try:
      mod_type = send_frame(spi, Pin(cs, Pin.OUT), 0x10, 0x01)[0]
      mods.append({'pin': cs, 'type': mod_type})
    except ChecksumError:
      pass
  return mods
