from module.module import Module, ModuleMode
from module.communication import send_frame


class IRModule(Module):
  NAME = 'Infrared'
  TYPE = 0x19

  def __init__(self, cs, mode=ModuleMode.NORMAL, vnode=None):
    super().__init__(cs, mode=mode, vnode=vnode)
    self.frames['on'] = {'slice': None, 'cmd': (0x00, 0x0c), 'args': 0}
    self.frames['off'] = {'slice': None, 'cmd': (0x00, 0x0d), 'args': 0}
    self.frames['read'] = {
      'slice': 0, 'cmd': (0x10, 0x0e), 'args': 1, 'mock_res': (1,)}
    self._bind_frames()
