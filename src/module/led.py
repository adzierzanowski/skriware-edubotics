from module.module import Module, ModuleMode
from module.communication import send_frame


class LEDModule(Module):
  NAME = 'LED'
  TYPE = 0x1f

  def __init__(self, cs, mode=ModuleMode.NORMAL, vnode=None):
    super().__init__(cs, mode=mode, vnode=vnode)
    self.frames['on'] = {'slice': None, 'cmd': (0x01, 0x0a), 'args': 1}
    self.frames['off'] = {'slice': None, 'cmd': (0x01, 0x0b), 'args': 1}
    self.frames['toggle'] = {'slice': None, 'cmd': (0x01, 0x0c), 'args': 1}
    self._bind_frames()
