import json

from machine import Pin
from module.communication import send_frame, spi, get_type
from utils import log


class ModuleMode:
  # Module is attached directly to the node
  # and communicates via SPI
  NORMAL = 'normal'

  # Module is controlled from a master device
  # via HTTP requests
  REMOTE = 'remote'

  # Hard-coded virtual module
  MOCK = 'mock'

class Module:
  NAME = 'Generic module'
  TYPE = 0xff

  def __init__(self, cs, mode=ModuleMode.NORMAL, vnode=None):
    self.mode = mode
    self.vnode = vnode

    if isinstance(cs, Pin):
      self.cs = cs
      self.cs_num = -1
    else:
      self.cs = Pin(cs, Pin.OUT)
      self.cs_num = cs

    self.cs.on()

    # This piece of code is rather complicated because it depends on reflection
    # a lot. What it does is it defines methods for sending frames based on the
    # frames list.
    # It's done this way because the list of frames is being used elsewhere and
    # defining it twice (one as a dict, second as methods) manually is pointless

    # Format:
    #     name:     -- name of the method
    #         slice -- number of bytes expected to be received from the module
    #         cmd   -- bytes of the specific command to be sent to the module
    #         args  -- number of additional arguments allowed to be passed
    self.frames = {
      'reset': {'slice': None, 'cmd': (0x00, 0x00), 'args': 0, 'mock_res': ()},
      'type': {'slice': 0, 'cmd': (0x10, 0x01), 'args': 0, 'mock_res': (0xff,)},
      'get_id': {'slice': 0, 'cmd': (0x10, 0x02), 'args': 0, 'mock_res': (0xf0,)},
      'set_id': {'slice': 0, 'cmd': (0x11, 0x02), 'args': 1, 'mock_res': (0xf0,)},
      'get_uid': {
        'slice': None,
        'cmd': (0xc0, 0x03),
        'args': 0,
        'mock_res': tuple([i for i in range(12)])},
      '_reset_complementary_interface': {
        'slice': None, 'cmd': (0x00, 0x04), 'args': 0, 'mock_res': ()},
      '_debug_n_increment': {
        'slice': None, 'cmd': (0x33, 0x05), 'args': 3, 'mock_res': (0x01, 0x02, 0x03)},
    }
    self._bind_frames()

  def _bind_frames(self):
    '''Assigns methods for sending specific frames to the modules'''

    for name, frame in self.frames.items():
      slice_ = frame.get('slice')

      def spi_sender(*args, name=name, slice_=slice_, frame=frame):
        argc = frame.get('args', 0)
        #print('name={} slice_={}'.format(name, slice_)) # NOTE: delete

        # This returns the response from the module up to `slice_` elements.
        # The args passed are also limited to `argc` elems.
        # Everything is passed to the method as single bytes, hence the `*`
        params = frame.get('cmd') + args[:argc]
        #print('argc:', argc, 'args:', args, 'params:', params) # NOTE: delete
        if slice_ is None:
          return self.send_frame(*params)
        else:
          return self.send_frame(*params)[slice_]

      def http_sender(*args, name=name, slice_=slice_, frame=frame):
        if self.vnode:
          argc = frame.get('args', 0)
          params = frame.get('cmd') + args[:argc]

          payload = json.dumps({
            'pin': self.cs_num,
            'frame': params
          })

          res = self.vnode._request('/send-frame', body=payload)
          if res._status == 200:
            if slice_ is None:
              return json.loads(res.body().decode('ascii'))
            else:
              return json.loads(res.body().decode('ascii'))[slice_]

          else:
            return res.body().decode('ascii')

        else:
          log.error('No vnode in the Module')

      def mock_sender(*args, name=name, slice_=slice_, frame=frame):
        return self.frames.get(name).get('mock_res', [0xff])[slice_]

      # assign the method
      if self.mode == ModuleMode.NORMAL:
        setattr(self, name, spi_sender)
      elif self.mode == ModuleMode.REMOTE:
        setattr(self, name, http_sender)
      elif self.mode == ModuleMode.MOCK:
        setattr(self, name, mock_sender)

    if self.mode == ModuleMode.MOCK:
      setattr(self, 'send_frame', self.mock_send_frame)

  def _serialize(self):
    return {
      'pin': self.cs_num,
      'name': getattr(self, 'NAME'),
      'type': getattr(self, 'TYPE')
    }

  def send_frame(self, *args):
    return send_frame(spi, self.cs, *args)

  def mock_send_frame(self, *args):
    try:
      cmd = args[0], args[1]

      for frame in self.frames.values():
        if frame.get('cmd', (0xff, 0xff))[:2] == cmd:
          return frame.get('mock_res', [])

    except:
      return []
    return []
