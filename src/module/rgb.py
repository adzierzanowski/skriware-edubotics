from module.module import Module, ModuleMode
from module.communication import send_frame


class RGBModule(Module):
  NAME = 'RGB Leds'
  TYPE = 0x10

  def __init__(self, cs, mode=ModuleMode.NORMAL, vnode=None):
    super().__init__(cs, mode=mode, vnode=vnode)
    self.frames['update'] = {'slice': None, 'cmd': (0x00, 0x0a), 'args': 0}
    self.frames['clear'] = {'slice': None, 'cmd': (0x00, 0x0b), 'args': 0}
    self.frames['set_pixel'] = {'slice': None, 'cmd': (0x04, 0x0c), 'args': 4}
    self._bind_frames()
