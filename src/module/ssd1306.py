from module.module import Module, ModuleMode
from module.communication import send_frame


class SSD1306Module(Module):
  NAME = 'Oled display'
  TYPE = 0x1d

  def __init__(self, cs, mode=ModuleMode.NORMAL, vnode=None):
    super().__init__(cs, mode=mode, vnode=vnode)
    self.frames['flip'] = {'slice': None, 'cmd': (0x00, 0x0a), 'args': 0}
    self.frames['clear'] = {'slice': None, 'cmd': (0x00, 0x0b), 'args': 0}
    self.frames['set_pixel'] = {'slice': None, 'cmd': (0x03, 0x0c), 'args': 3}
    self._bind_frames()
