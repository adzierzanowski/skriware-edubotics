from module.module import Module, ModuleMode
from module.communication import send_frame


class SwitchModule(Module):
  NAME = 'Switch'
  TYPE = 0x1e

  def __init__(self, cs, mode=ModuleMode.NORMAL, vnode=None):
    super().__init__(cs, mode=mode, vnode=vnode)
    self.frames['switch_count'] = {'slice': 0, 'cmd': (0x10, 0x0a), 'args': 0}
    self.frames['on'] = {'slice': None, 'cmd': (0x01, 0x0b), 'args': 1}
    self.frames['off'] = {'slice': None, 'cmd': (0x01, 0x0c), 'args': 1}
    self.frames['toggle'] = {'slice': None, 'cmd': (0x01, 0x0d), 'args': 1}
    self._bind_frames()
