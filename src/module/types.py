from module.module import Module
from module.ssd1306 import SSD1306Module
from module.ir import IRModule
from module.switch import SwitchModule
from module.rgb import RGBModule
from module.led import LEDModule


GENERIC_MODULE = Module
RGB_MODULE = RGBModule
OLED_MODULE = SSD1306Module
IR_MODULE = IRModule
SWITCH_MODULE = SwitchModule
LED_MODULE = LEDModule

MODULE_TYPES = {
  0x10: RGBModule,
  0x11: Module, #'QTR (Reflectance sensor)',
  0x12: Module, #'HC-SR04 (Ultrasonic sensor)',
  0x13: Module, #'Gripper module',
  0x14: Module, #'DC Motor',
  0x15: Module, #'BH1750 (Light sensor)',
  0x16: Module, #'Buzzer',
  0x17: Module, #LED Matrix',
  0x18: Module, #'Button',
  0x19: IRModule,
  0x1a: Module, #'Color sensor',
  0x1b: Module, #'Flow sensor',
  0x1c: Module, #'MQ-type sensor',
  0x1d: SSD1306Module,
  0x1e: SwitchModule,
  0x1f: LEDModule,
  0xff: Module,
}
