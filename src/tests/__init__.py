from tests.http.http import HTTPRequestTest, HTTPResponseTest
from tests.utils.ping import ICMPFrameTest
from tests.utils.queue import QueueTest
from utils import config, log

tests = {
  HTTPRequestTest,
  HTTPResponseTest,
  QueueTest,
  ICMPFrameTest
}

def run_all():
  current_loglevel = config.log_level
  config.log_level = log.ERROR

  try:
    for test in tests:
      test().run()
  finally:
    config.log_level = current_loglevel
