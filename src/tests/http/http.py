from http import http

from utils import utest

raw_request = '\r\n'.join((
  'POST /route?param1=foo&param2=bar HTTP/1.1',
  'Host: 192.168.4.2',
  'Connection: keep-alive',
  'User-Agent: Edubotics (ESP32)',
  'Content-Type: text/html',
  'Content-Length: 36',
  'Cache-Control: no-cache'
  '',
  '',
  '<body>The body of the request</body>'
)).encode('ascii')

raw_response = '\r\n'.join((
  'HTTP/1.1 500 Internal Server Error',
  'Content-Type: text/html',
  'Content-Length: 46',
  'Connection: close',
  '',
  '<h1>500 Internal Server Error</h1><p>Oops!</p>'
  )).encode('ascii')

class HTTPTest(utest.UnitTest):
  def test_redirect(self):
    self.assert_equal(
      http.redirect('http://google.com'),
      '\r\n'.join((
        'HTTP/1.1 301 Moved Permanently',
        'Content-Type: text/plain',
        'Content-Length: 0',
        'Connection: close',
        'Location: http://google.com',
        '',
        ''
      )).encode('ascii')
    )

  def test_status_response(self):
    self.assert_equal(
      http.status_response(404),
      '\r\n'.join((
      'HTTP/1.1 404 Not Found',
      'Content-Type: text/html',
      'Content-Length: 22',
      'Connection: close',
      '',
      '<h1>404 Not Found</h1>'
      )).encode('ascii')
    )
  
  def test_status_response_with_message(self):
    self.assert_equal(
      http.status_response(500, 'Oops!'),
      '\r\n'.join((
      'HTTP/1.1 500 Internal Server Error',
      'Content-Type: text/html',
      'Content-Length: 46',
      'Connection: close',
      '',
      '<h1>500 Internal Server Error</h1><p>Oops!</p>'
      )).encode('ascii')
    )

  def test_request(self):
    self.assert_equal(
      http.request(
        '/route?param1=foo&param2=bar',
        '192.168.4.2',
        '<body>The body of the request</body>',
        headers={
          'Cache-Control': 'no-cache'
        },
        method='POST',
        mime_type=http.MIME['html']
      ),
      '\r\n'.join((
        'POST /route?param1=foo&param2=bar HTTP/1.1',
        'Host: 192.168.4.2',
        'Connection: keep-alive',
        'User-Agent: Edubotics (ESP32)',
        'Content-Type: text/html',
        'Content-Length: 36',
        'Cache-Control: no-cache'
        '',
        '',
        '<body>The body of the request</body>'
      )).encode('ascii')
    )
  
class HTTPRequestTest(utest.UnitTest):
  def setup(self):
    self.req = http.HTTPRequest.parse(raw_request)

  def test_parse(self):
    self.assert_equal(self.req.method, 'POST')
    self.assert_equal(self.req._host, '192.168.4.2')
    self.assert_equal(self.req.route, '/route?param1=foo&param2=bar')
  
  def test_route_params(self):
    self.assert_equal(self.req.route_params(), {'param1': 'foo', 'param2': 'bar'})

  def test_status_line(self):
    self.assert_equal(
      self.req.status_line(), 'POST /route?param1=foo&param2=bar HTTP/1.1')
    
  def test_headers(self):
    self.assert_equal(self.req._headers['Host'], '192.168.4.2')
    self.assert_equal(self.req._headers['Connection'], 'keep-alive')
    self.assert_equal(self.req._headers['User-Agent'], 'Edubotics (ESP32)')
    self.assert_equal(self.req._headers['Content-Type'], 'text/html')
    self.assert_equal(self.req._headers['Content-Length'], '36')
    self.assert_equal(self.req._headers['Cache-Control'], 'no-cache')

  def test_body(self):
    self.assert_equal(self.req.body(), b'<body>The body of the request</body>')

class HTTPResponseTest(utest.UnitTest):
  def setup(self):
    self.res = http.HTTPResponse.parse(raw_response)

  def test_parse(self):
    self.assert_equal(self.res._status, 500)

  def test_redirect(self):
    redirect = http.HTTPResponse.redirect('https://google.com')
    self.assert_equal(redirect._headers['Location'], 'https://google.com')
    self.assert_equal(redirect._status, 301)
