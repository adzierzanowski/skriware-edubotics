from utils import utest
from utils.ping import ICMPFrame


payload = b''.join((
  b'\x00\x00\x00\x00\x9e\x1c\xacj',
  b'\x00\x00\x00\x00\rw\xd7\t',
  b'\x00\x00\x00\x00\xfb4\x19_',
  b'\x00\x00\x00\x00w\x83\x1c@',
  b'\x00\x00\x00\x00\x05\xff\x02\x8b',
  b'\x00\x00\x00\x00\x11\xa1\xbd}',
  b"\x00\x00\x00\x00\xe1\xc0'\xc7"
))

raw_req = b''.join((
  b'\x08\x00?n\x00\x00\x00\x01',
  payload
))

raw_res = b''.join((
  b'\x00\x00Gn\x00\x00\x00\x01',
  payload
))

class ICMPFrameTest(utest.UnitTest):
  def setup(self):
    self.req = ICMPFrame.parse(raw_req)
    self.res = ICMPFrame.parse(raw_res)

  def test_parse(self):
    self.assert_equal(self.req.type, 8)
    self.assert_equal(self.req.code, 0)
    self.assert_equal(self.req.id_, 0)
    self.assert_equal(self.req.seq, 1)

    self.assert_equal(self.res.type, 0)
    self.assert_equal(self.res.code, 0)
    self.assert_equal(self.res.id_, 0)
    self.assert_equal(self.res.seq, 1)

  
  def test_is_response_to(self):
    self.assert_true(self.res.is_response_to(self.req))
