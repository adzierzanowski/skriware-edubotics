from utils import utest
from utils.queue import Queue


class QueueTest(utest.UnitTest):
  def setup(self):
    self.queue = Queue()

    for i in range(10):
      self.queue.put(i)
  
  def test_container(self):
    for i in range(10):
      self.assert_in(i, self.queue._container)
  
  def test_get(self):
    self.assert_equal(self.queue.get(), 0)
