from utils import utest


class UTestTest(utest.UnitTest):
  def setup(self):
    print('setup')
  
  def tear_down(self):
    print('tear down')

  def test_passing(self):
    self.assert_equal('banana', 'banana')
  
  def test_failing(self):
    self.assert_false(True)
  
  def test_in(self):
    self.assert_in(10, range(18))
