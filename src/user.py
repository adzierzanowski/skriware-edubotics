'''
This is the user script. You can write your own programs here.
'''

import time
# Tutaj będzie import z czegoś bardziej sensownego niż utils.config
# To jest tylko taka prowizorka na chwilę, bo import z boot.py jest
# z pewnych powodów kłopotliwy
from utils.config import master
from module.types import *

print('Skanuję moduły...')
master.scan()

print('Sprawdzam, czy wszystkie potrzebne moduły są podłączone')
if master.validate([LED_MODULE, IR_MODULE, RGB_MODULE]):
    print('  Tak, moduły są podłączone')
    print('Przystępuję zatem do akcji')

    print('Pobieram wszystkie moduły typu LED')
    leds = master.modules(LED_MODULE)

    # Tutaj możemy używać naszych modułów na dwa sposoby
    # Jeżeli już sprawdziliśmy przez master.validate, że te moduły rzeczywiście są
    # to wystarczy iterować po liście modułów albo przypisać do zmiennej
    # pierwszy moduł: `sw = switch[0]`
    for led in leds:
        print('Włączam przełącznik')
        led.on(1) # Włączamy przełącznik nr 1 modułu led

    print('Czekam jedną sekundę')
    time.sleep(1)

    # Jeżeli jednak nie zrobiliśmy master.validate, to możemy sprawdzić, czy lista
    # switchy ma jakieś elementy
    if leds:
        led = leds[0]
        print('Przełączam przełącznik na przeciwną pozycję')
        led.toggle(1) # Zmieniamy stan przełącznika nr 1 pierwszego modułu na liście
    else:
        print('Ojejku, nie ma żadnego switcha na liście modułów')

    # Pora na przetestowanie pozostałych modułów
    irs = master.modules(IR_MODULE)
    rgbs = master.modules(RGB_MODULE)
    if irs and rgbs:
        # Moduł IR zawiera w sobie odbiornik i nadajnik
        ir = irs[0]
        rgb = rgbs[0]

        print('Włączam diodę podczerwoną')
        ir.on()
        time.sleep(1)

        if ir.read(): # Jeżeli odbiornik IR wykrywa światło podczerwone z diody
            print('Dioda świeci, więc jeszcze dorzucamy światełka')
            led.toggle(1)
            # Zamiast 0xff może być 255, jest to maksymalna wartość
            rgb.set_pixel(0, 0xff, 0, 0) # czerwony
            rgb.update() # Po ustawieniu kolorów przesyłamy je do paska LED

            print('Już odczytaliśmy diodę, więc możemy ją wyłączyć')
            ir.off()

            print('Czekamy sekundę i wyłączamy światełka')
            time.sleep(1)
            led.off(1)
            rgb.reset() # Wyłączamy wszystkie ledy z paska
            rgb.update() # Też trzeba zupdate'ować
        else:
            print('Dioda najwyraźniej nie świeci')
    else:
        print('Brakuje niektórych modułów')
else:
    print('  Brakuje niektórych modułów')
