'''Simple ANSI formatting'''

end = '\033[0m'

def fg(color, text):
  return '\033[38;5;{}m{}\033[0m'.format(color, text)

def bg(color, text):
  return '\033[48;5;{}m{}\033[0m'.format(color, text)
