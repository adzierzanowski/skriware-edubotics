'''Configuration'''

# Byte-limit for one request over socket connection
data_limit = 1024

# Prefix for the wifi network created by the master
master_prefix = 'Edubotics:master'

# Standard master IP
master_ip = '192.168.4.1'

# Standard connection timeout
timeout = 5

# Connection doctor interval
doctor_interval = 15.0

# Logging
log_level = 2
queue_logs = False

# HTTP server root directory
public_dir = 'www'

default_network_color = 'blue'
master_led_color = (32, 32, 0)
node_led_color = (32, 0, 32)

master = None
