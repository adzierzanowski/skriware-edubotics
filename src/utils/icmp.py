import random
import struct
import socket
import time

from utils import log
from utils import config


IPPROTO_ICMP = socket.IPPROTO_ICMP if hasattr(socket, 'IPPROTO_ICMP') else 1

class ICMPFrame:
  STRUCT_FMT = '>BBHHH' + 'Q' * 7

  def __init__(self, type_, code, checksum, id_, seq, *payload):
    self.type = type_
    self.code = code
    self.checksum = checksum
    self.id_ = id_
    self.seq = seq
    self.payload = payload

  @staticmethod
  def parse(raw):
    fields = struct.unpack(ICMPFrame.STRUCT_FMT, raw)
    return ICMPFrame(*fields)

  def raw(self, clear_checksum=False):
    return struct.pack(
      ICMPFrame.STRUCT_FMT,
      self.type,
      self.code,
      0 if clear_checksum else self.checksum,
      self.id_,
      self.seq,
      *self.payload
    )
  
  def calculate_checksum(self, apply_checksum=True):
    raw = self.raw(clear_checksum=True)

    # Sum 16-bit chunks
    words = [int.from_bytes(raw[i:i+2], 'big') for i in range(0, len(raw), 2)]
    checksum = sum(words) 

    # Add carry bytes
    while checksum > 0x10000:
      checksum = (checksum & 0xffff) + (checksum >> 16)

    # Invert the result
    checksum ^= 0xffff

    if apply_checksum:
      self.checksum = checksum

    return checksum
  
  def validate_checksum(self) -> bool:
    checksum = self.calculate_checksum(apply_checksum=False)
    return self.checksum == checksum
  
  def is_response_to(self, req: ICMPFrame):
    return all((
      self.validate_checksum(),
      req.validate_checksum(),
      self.id_ == req.id_,
      self.seq == req.seq,
      self.payload == req.payload
    ))

def ping(host, timeout=2.0, seq_lim=4):
  addr = socket.getaddrinfo(host, 1)[0][-1][0]

  if not addr:
    return False

  try:
    s = socket.socket(socket.AF_INET, socket.SOCK_RAW, IPPROTO_ICMP)
    s.setblocking(False)
    s.settimeout(timeout)
    s.connect((addr, 1)) # 1 = dummy port
  except OSError as e:
    log.log_exception(e, __file__)
    return False

  for seq in range(seq_lim):
    try:
      payload = [
        random.getrandbits(32) << 32 | random.getrandbits(32) for i in range(7)
      ]
      echo_req = ICMPFrame(
        8, 0,
        random.getrandbits(16), # random ID
        0, # checksum, 0 for now
        seq,
        *payload
      )
      echo_req.calculate_checksum()

      s.send(echo_req.raw())
      res = s.recv(config.data_limit)
      echo_res = ICMPFrame.parse(res[20:]) # Skip IPv4 header (20 bytes)
      s.close()

      # Check for data consistency
      if echo_res.is_response_to(echo_req):
        return True
      time.sleep(1)

    except OSError:
      continue

    finally:
      s.close()

  return False
