'''Logger'''

import errno
import re
import time

from utils import ansi, queue


try:
  from utils import config
except ImportError:
  class config:
    log_level = 4
    queue_logs = False

NONE = 4
DEBUG = 3
WARNING = 2
ERROR = 1
CRITICAL = 0

_ansi = {
  NONE: '\033[0m',
  DEBUG: '\033[94mDEBUG: ',
  WARNING: '\033[93mWARNING: ',
  ERROR: '\033[91mERROR: ',
  CRITICAL: '\033[101m\033[97mCRITICAL: '
}

log_q = queue.Queue(maxlen=32)

def log(level, msg):
  '''Prints a message if matches the log level'''

  if level <= config.log_level:
    print('({}) {}{}\033[0m'.format(
      time.time(),
      _ansi[level],
      msg
    ))

  message = re.sub('\x1b\[.*?m', '', msg)
  if config.queue_logs:
    log_q.put('({}) [{}] {}'.format(
      time.ticks_ms(),
      level,
      message
    ))

def debug(msg):
  log(DEBUG, msg)

def warning(msg):
  log(WARNING, msg)

def error(msg):
  log(ERROR, msg)

def exception(*args, **kwargs):
  log_exception(*args, **kwargs)

def log_exception(e: Exception, fname: str):
  classname = e.__class__.__name__
  ecode = ''
  if classname == 'OSError':
    try:
      ecode = errno.errorcode[e.args[0]] + ' '
    except KeyError:
      ecode = 'UNKNOWN ({})'.format(e.args[0])

  try:
    args = '\n'.join(e.args)
  except TypeError:
    args = ''

  log(ERROR, '{}: {}: {}{}'.format(
    fname, e.__class__.__name__, ecode, args))
  
def log_http_response(res, host, port):
  log(DEBUG, '{}res {}:{} {}'.format(
    ansi.end,
    host, port,
    repr(res)
  ))
