from utils import log
from http.http import request, HTTPRequest


def ping(host, timeout=2.0, seq_lim=4, iface=None):
  ok = False
  if iface:
    ping_req = HTTPRequest(host, route='/ping')
    for i in range(seq_lim):
      try:
        res = request(host, iface, ping_req)
        if res.body() == b'pong':
          ok = True
          break
        else:
          continue
      except OSError as e:
        log.log_exception(e, __file__)
        continue

  return ok
