'''FIFO queue for threads'''

import _thread


class Queue:
  def __init__(self, maxlen=None):
    self._container = []
    self._container_lock = _thread.allocate_lock()
    self.max_length = float('inf') if maxlen is None else maxlen
  
  def get(self):
    with self._container_lock:
      try:
        return self._container.pop(0)
      except IndexError:
        return None

  def put(self, item):
    with self._container_lock:
      if len(self._container) >= self.max_length:
        self._container.pop(0)
      self._container.append(item)
  
  def empty(self):
    with self._container_lock:
      return len(self._container) <= 0
