'''Socket server base class'''

import socket
import time

import _thread
from utils import log
from utils.queue import Queue
from machine import reset


class Server:
  def __init__(self, iface, client_handler, port):
    self.iface = iface
    self.client_handler = client_handler
    self.port = port
    self.queue = Queue()
    self.errcnt = 0

  def worker(self):
    while True:
      qitem = self.queue.get()
      if qitem is not None:
        cl, addr = qitem
        self.client_handler(cl, addr)

  def listen(self, max_connections=3):
    s = socket.socket()
    s.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
    s.bind(('0.0.0.0', self.port))
    s.listen(max_connections)

    for i in range(max_connections):
      _thread.start_new_thread(self.worker, ())

    while True:
      try:
        cl, addr = s.accept()
        self.queue.put((cl, addr))
      except OSError as e:
        log.log_exception(e, __file__)
        self.errcnt += 1
        if self.errcnt == 10:
          reset()

      else:
        self.errcnt = 0
