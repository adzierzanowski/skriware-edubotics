'''Status indicator'''

import time

import _thread
from machine import Pin
from neopixel import NeoPixel
from utils import config, edunet


class Status:
  def __init__(self):
    self.np = NeoPixel(Pin(14), 2)
    self.status_led1_blink = False
    self.status_led2_blink = False
    self.status_led1_color = (0, 0, 0)
    self.status_led2_color = (0, 0, 0)
    _thread.start_new_thread(self.update_status, ())
  
  def disconnected(self, color):
    self.status_led1_blink = True
    self.status_led1_color = edunet.colors[color]
  
  def connected(self, color):
    self.status_led1_blink = False
    self.status_led1_color = edunet.colors[color]
  
  def node(self):
    self.status_led2_blink = False
    self.status_led2_color = config.node_led_color

  def master(self):
    self.status_led2_color = config.master_led_color

  def update_status(self):
    blink_off = False
    while True:
      self.np[0] = self.status_led1_color
      self.np[1] = self.status_led2_color

      if self.status_led1_blink:
        if blink_off:
          self.np[0] = (0, 0, 0)
          blink_off = False
        else:
          blink_off = True

      if self.status_led2_blink:
        if blink_off:
          self.np[1] = (0, 0, 0)
          blink_off = False
        else:
          blink_off = True

      self.np.write()
      time.sleep(0.05)

status = Status()
