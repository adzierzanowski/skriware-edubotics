'''Microframework for unit testing.'''

import time

from utils import ansi, log


class UnitTest:
  def setup(self):
    pass
  
  def tear_down(self):
    pass

  def assert_(self, condition, msg=None):
    try:
      assert(condition)
    except AssertionError:
      if msg is None:
        raise AssertionError('{}'.format(condition))
      else:
        raise AssertionError(msg)

  def assert_equal(self, got, expected):
    self.assert_(
      got == expected,
      '\n{:10}: {}\n{:10}: {}'.format('expected', expected, 'got', got)
    )
  
  def assert_not_equal(self, got, not_expected):
    self.assert_(
      got != not_expected,
      '\n{:15}: {}\n{:15}:{}'.format('not expected', not_expected, 'got', got)
    )
  
  def assert_true(self, thing):
    self.assert_(thing == True, '{} is not True'.format(thing))
  
  def assert_false(self, thing):
    self.assert_(thing == False, '{} is not False'.format(thing))

  def assert_in(self, needle, haystack):
    self.assert_(needle in haystack, '{} not in {}'.format(needle, haystack))

  def run(self):
    start = time.ticks_ms()
    print('Running {}'.format(self.__class__.__name__))

    self.setup()

    test_count = 0
    tests_passed = 0
    tests_failed = 0

    for name in dir(self):
      obj = getattr(self, name)
      if name.startswith('test') and callable(obj):
        test_count += 1

        try:
          obj()
          print(ansi.fg(2, '.'), end='')
          tests_passed += 1

        except AssertionError as e:
          print()
          log.error('`{}` failed:'.format(name))
          log.log_exception(e, __file__)
          print(ansi.fg(1, 'F'), end='')
          tests_failed += 1

        except Exception as e:
          log.log_exception(e, __file__)
          print(ansi.fg(1, 'E'), end='')
          tests_failed += 1
          print()
          self.tear_down()
          return

    print()
    self.tear_down()

    print('Ran {} test(s) in {} ms.'.format(test_count, time.ticks_ms() - start))
    if tests_failed == 0:
      print(ansi.fg(2, 'OK'))
    else:
      print('{} failure(s).'.format(tests_failed))
    print()
