let nodes = {};
let actions = {};

function updateNodeList()
{
  fetch('/nodes').then(res => {
    res.json().then(json => {
      // Loop over fetched nodes
      for (let jsonIP in json)
      {
        let jsonNode = json[jsonIP];
        nodes[jsonIP] = jsonNode;
        nodes[jsonIP].online = true;
      }

      // Loop over existing nodes
      // if there is no equivalent in the fetched json
      // then mark it as offline node
      for (let nodeIP in nodes)
      {
        if (!(nodeIP in json))
        {
          nodes[nodeIP].online = false;
        }

        if (!(nodeIP in actions))
        {
          fetch('/available?redirect=' + nodeIP).then(actionRes => {
            actionRes.json().then(actionJson => {
              actions[nodeIP] = actionJson;
            }).catch(err => console.log('error:', err));
          });
        }
      }

      updateNodeView();
    });
  }).catch(err => console.log('error:', err));
}

function updateNodeView()
{
  let table = document.getElementById('node-table');
  let tableBody = table.querySelector('tbody');

  for (nodeKey in nodes)
  {
    let node = nodes[nodeKey];
    let nodeTr = tableBody.querySelector('tr[data-uid="' + node.name + '"]');
    
    // If there is no such node in view, then create it
    if (nodeTr === null)
    {
      let tr = document.createElement('tr');
      tr.setAttribute('data-ip', node.ip);
      tr.setAttribute('data-uid', node.name);
      tableBody.appendChild(tr);

      let ipTd = document.createElement('td');
      ipTd.setAttribute('class', 'ip');
      let ipContent = document.createTextNode(node.ip);
      ipTd.appendChild(ipContent);
      tr.appendChild(ipTd);

      let uidTd = document.createElement('td');
      uidTd.setAttribute('class', 'uid');
      let uidContent = document.createTextNode(node.name);
      uidTd.appendChild(uidContent);
      tr.appendChild(uidTd);

      let typeTd = document.createElement('td');
      typeTd.setAttribute('class', 'type');
      let typeContent = document.createTextNode(node.type);
      typeTd.appendChild(typeContent);
      tr.appendChild(typeTd);

      let onlineTd = document.createElement('td');
      onlineTd.setAttribute('class', 'online');
      let onlineContent = document.createTextNode(node.online);
      onlineTd.appendChild(onlineContent);
      tr.appendChild(onlineTd);

      let actionsTd = document.createElement('td');
      actionsTd.setAttribute('class', 'actions');
      tr.appendChild(actionsTd);

      let actionResultTd = document.createElement('td');
      actionResultTd.setAttribute('class', 'action-result');
      tr.appendChild(actionResultTd);
    }

    // If a view for the node already exists, then just update the values
    else
    {
      nodeTr.querySelector('td.ip').innerText = node.ip;
      nodeTr.querySelector('td.uid').innerText = node.name;
      nodeTr.querySelector('td.type').innerText = node.type;
      nodeTr.querySelector('td.online').innerText = node.online;

      let actionsTd = nodeTr.querySelector('td.actions');
      if (actionsTd.innerText === '')
      {
        let actionSelect = document.createElement('select');
        if (actions[node.ip] !== undefined)
        {
          if (actions[node.ip].getters !== undefined)
          {
            for (let getter of actions[node.ip].getters)
            {
              let getterOption = document.createElement('option');
              getterOption.setAttribute('value', '/get/' + getter + '/?redirect=' + node.ip)
              getterOption.appendChild(document.createTextNode(getter));
              actionSelect.appendChild(getterOption);
            }
          }

          actionsTd.appendChild(actionSelect);

          let actionsButton = document.createElement('button');
          actionsButton.appendChild(document.createTextNode('perform'));
          actionsButton.addEventListener('click', e => {

            fetch(actionsTd.querySelector('select').selectedOptions[0].value).then(res => {
              res.text().then(text => {
                nodeTr.querySelector('td.action-result').innerText = text;
              });
            });
          }, false);
          actionsTd.appendChild(actionsButton);
        }
      }
    }
  }
}

function clickAll()
{
  setInterval(() => {
    document.querySelectorAll('button').forEach(btn => btn.click());
  }, 1000);
}

function getLogs()
{
  let logView = document.querySelector('#logs');

  fetch('/get/logs').then(res => {
    res.text().then(resVal => {
      logView.value += resVal + '\n';
    });
  });
}

let logView = document.querySelector('#logs');
logView.value = '';
setInterval(getLogs, 5000);
setInterval(updateNodeList, 1500);
