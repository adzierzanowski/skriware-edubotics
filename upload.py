#!/usr/bin/env python3

import argparse
import logging
import os
import pathlib
import subprocess
import sys
from enum import Enum

from ampy.files import Files
from ampy.pyboard import Pyboard, PyboardError


def setup_logger():
  logging.getLogger().setLevel(logging.INFO)
  logger = logging.getLogger('upload')
  ch = logging.StreamHandler()
  ch.setFormatter(logging.Formatter(f'%(levelname)s -- %(message)s'))
  logger.addHandler(ch)
  return logger

logger = setup_logger()

class Uploader:
  def __init__(self, port):
    self.port = port
    self.pyboard = Pyboard(port)
    self.fh = Files(self.pyboard)

  def upload_cmd(self, src, dst):
    return ['ampy', '--port', self.port, 'put', src, dst]

  def upload_file(self, src, dst=None):
    p = pathlib.Path(src)
    if dst is None:
      # Skip the first segment of the path
      dst_path = os.path.join(*p.parts[1:])
    else:
      dst_path = dst

    logger.info(f'uploading {str(dst_path):20} to {self.port}')
    with open(src, 'r') as f:
      self.fh.put(dst_path, f.read())

  def exists(self, path):
    try:
      ls = self.fh.ls(directory=path, long_format=False)
      return True
    except RuntimeError:
      return False

  def mkdir(self, path):
    p = pathlib.Path(path)
    dst_path = os.path.join(os.path.join(*p.parts[1:]))
    if not self.exists(dst_path):
      logger.info(f'creating directory {dst_path}')
      self.fh.mkdir(dst_path)

  def remove_files(self):
    logger.info('removing existing files')
    self.fh.rmdir('/', missing_okay=True)

  def upload_dir(self, path):
    self.mkdir(path)
    for fname in os.listdir(path):
      fname_path = os.path.join(path, fname)
      if os.path.isdir(fname_path):
        self.mkdir(fname_path)
        self.upload_dir(fname_path)
      else:
        self.upload_file(fname_path)


if __name__ == '__main__':
  parser = argparse.ArgumentParser('upload')
  parser.add_argument(
    '-d', '--directory',
    help='directory to upload',
    type=str
  )
  parser.add_argument(
    '-m', '--mode',
    help='upload mode',
    type=str
  )
  parser.add_argument(
    '-p', '--port',
    help='serial port of the device',
    type=str,
    required=True
  )
  parser.add_argument(
    '-r', '--remove-files',
    help='remove files from board before the upload',
    action='store_true'
  )
  parser.add_argument(
    '-s', '--specific-node',
    type=str,
    help='specific node type',
    default='node'
  )
  parser.add_argument(
    '-t', '--tests',
    help='upload tests too',
    action='store_true'
  )
  args = parser.parse_args()

  uploader = Uploader(args.port)

  if args.remove_files:
    uploader.remove_files()

  if not args.mode:
    if args.directory:
      uploader.upload_dir(args.directory)
    else:
      logger.warning('no upload mode is chosen')

  if args.mode in ('node', 'master'):
    uploader.upload_dir('src/http')
    uploader.upload_dir('src/utils')
    uploader.upload_dir('src/www')
    uploader.upload_dir('src/device')
    uploader.upload_dir('src/module')

    if args.mode == 'node':
      uploader.upload_file(f'src/device/boot_{args.specific_node}.py', 'boot.py')
    else:
      uploader.upload_file(f'src/device/boot_master.py', 'boot.py')
      uploader.upload_file(f'src/user.py', 'user.py')
  elif args.mode:
    logger.warning('no such upload mode')

  if args.tests:
    uploader.upload_dir('src/tests')
